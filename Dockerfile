FROM alpine

RUN apk add ansible libffi-dev musl-dev gcc openssl-dev python3-dev && \
    python3 -m pip install pip paramiko --upgrade

WORKDIR /rootfs